# Trends in Torrential Flooding in the Austrian Alps

> A combination of climate change, exposure dynamics, and mitigation measures 

<div align="center">
  <img width="368" height="325" src="https://boku.ac.at/fileadmin/_processed_/c/c/csm_IMG_4623_Kopie_a024955d0f.jpg">
</div>

## Literature

### Trends

- Schlögl, Fuchs, Scheidl & Heiser (2022): **Trends in torrential flooding in the Austrian Alps: A combination of climate change, exposure dynamics, and mitigation measures**. *Climate Risk Management*, 32, 100294. [doi:10.1016/j.crm.2021.100294](https://doi.org/10.1016/j.crm.2021.100294).
- Heiser, Schlögl, Scheidl, & Fuchs (2022). **Fallacies in Debris Flow Trend Analysis.** Comment on “Hydrometeorological triggers of periglacial debris flows in the Zermatt Valley (Switzerland) since 1864” by Michelle Schneuwly-Bollschweiler and Markus Stoffel. *Journal of Geophysical Research: Earth Surface*, 127, e2021JF006562. [doi:10.1029/2021JF006562](https://doi.org/10.1029/2021JF006562).
- Heiser, Hübl & Scheidl (2019): **Completeness analyses of the Austrian torrential event catalog**. *Landslides*, 16(11), 2115–2126. [doi:10.1007/s10346-019-01218-3](https://doi.org/10.1007/s10346-019-01218-3).

### Vulnerability

- Papathoma-Köhle, Schlögl, Dosser, Roesch, Borga, Erlicher, Keiler & Fuchs (2022): **Physical vulnerability to dynamic flooding: Vulnerability curves and vulnerability indices**. *Journal of Hydrology*, 607, 127501. [doi:10.1016/j.jhydrol.2022.127501](https://doi.org/10.1016/j.jhydrol.2022.127501).
- Papathoma-Köhle, Schlögl & Fuchs S (2019): **Vulnerability indicators for natural hazards: an innovative selection and weighting approach**. *Scientific Reports*, 9, 15026. [doi:10.1038/s41598-019-50257-2](https://doi.org/10.1038/s41598-019-50257-2).
- Fuchs, Heiser, Schlögl, Zischg, Papathoma-Köhle, & Keiler (2019): **Short communication: a model to predict flood loss in mountain areas**. *Environmental Modelling and Software*, 117, 176-180. [doi:10.1016/j.envsoft.2019.03.026](https://doi.org/10.1016/j.envsoft.2019.03.026).

## Press Release
- [Klimawandel und schadbringende Muren](https://www.zamg.ac.at/cms/de/klima/news/klimawandel-und-schadbringende-muren)
- [Überschwemmungen vorbeugen: BOKU verbessert Methoden zur Risikoeinschätzung an Gebäuden](https://boku.ac.at/universitaetsleitung/rektorat/stabsstellen/oeffentlichkeitsarbeit/themen/presseaussendungen/presseaussendungen-2022/17022022-ueberschwemmungen-vorbeugen-boku-verbessert-methoden-zur-risikoeinschaetzung-an-gebaeuden)

## Presentation
- Created in [RMarkdown](https://rmarkdown.rstudio.com/) / [ioslides](https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html).
- Served as GitLab-pages at <https://rexthor.gitlab.io/trends-in-torrential-flooding>.
